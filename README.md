[![Licence](https://img.shields.io/badge/license-CC%20BY--NC--ND%204.0-blue.svg)](LICENSE)

## A propos du projet

Solitaire Chess est un jeu d'échecs se jouant à 1 joueur. À travers 60 défis de difficulté croissante, il faudra capturer toutes les pièces du plateau (sauf une) en suivant les règles de déplacement des échecs classiques.
Le jeu propose aussi de créer des niveaux et d'y jouer. Ainsi que de personnaliser l'affichage global.

Pour installer le projet :

1. Ouvrez un éditeur de code Java
2. Compilez les sources
3. Executez le code

Pour voir mes autres projets, rendez vous sur mon [portfolio](https://raphael-octau.fr/en).

## Licence

&copy; 2019 Raphaël OCTAU

Ce travail est sous licence [Creative Commons (Attribution - Pas d'Utilisation Commerciale - Pas de Modification) 4.0 International](LICENSE).