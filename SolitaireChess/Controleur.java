package SolitaireChess;

import SolitaireChess.ihm.FenetreChoixNiveau;
import SolitaireChess.ihm.FenetreAccueil;
import SolitaireChess.ihm.FenetreJeu;
import SolitaireChess.metier.Plateau;
import SolitaireChess.metier.Position;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;

public class Controleur {

    private static Plateau p;
    private static FenetreJeu f;
    private static FenetreAccueil a;

    private Icon imgRoi, imgReine, imgTour, imgCavalier, imgFou, imgPion;
    private Color coul;

    private ArrayList<Position> alPos;
    private Position posDepart;

    private boolean aide = true;

    /**
     * Cree un nouveau Controleur
     */
    public Controleur() {
        a = new FenetreAccueil(this);
        this.imgRoi = new ImageIcon("./ressources/Images/roi.gif");
        this.imgReine = new ImageIcon("./ressources/Images/reine.gif");
        this.imgTour = new ImageIcon("./ressources/Images/tour.gif");
        this.imgCavalier = new ImageIcon("./ressources/Images/cavalier.gif");
        this.imgFou = new ImageIcon("./ressources/Images/fou.gif");
        this.imgPion = new ImageIcon("./ressources/Images/pion.gif");
        this.coul = null;
    }

    /**
     * Lance une partie
     * @param bCharger True = Afficher la fenetre de choix de niveau
     */
    public void jouer(boolean bCharger) {
        f = new FenetreJeu(this);
        p = new Plateau(f);
        f.actualiser(p);
        f.majCoul(p.getNiveau());
        f.setNiveau(p.getDifficulte());
        if(bCharger)
            new FenetreChoixNiveau(p, this);
    }

    /**
     * @return le plateau actuel
     */
    public Plateau getPlateau() { return this.p; }

    /**
     * Lance une nouvelle partie
     * (reinitialise la progression)
     */
    public void nouvellePartie() {
        reinitProgression();
        nouvNiveau(1);
    }

    /**
     * Recharge le niveau actuel
     */
    public void recharger() {
        p.initPlateau(p.getNiveau());
        p.setNbDeplacements(p.getNbDeplacements()+2);
        f.setNbDeplacements(p.getNbDeplacements());
        f.actualiser(p);
        f.reinitAide();
        f.majCoul(p.getNiveau());
        f.setNiveau(p.getDifficulte());
    }

    /**
     * Charge le niveau suivant
     */
    public void nivSuivant() {
        p.initPlateau(p.getNiveau()+1);
        p.setNbDeplacements(0);
        f.setNbDeplacements(0);
        f.actualiser(p);
        f.majCoul(p.getNiveau());
        f.setNiveau(p.getDifficulte());
        defiComplet();
    }

    /**
     * Charge un nouveau niveau
     * @param niv le niveau a charger
     */
    public void nouvNiveau(int niv) {
        p.initPlateau(niv);
        p.setNbDeplacements(0);
        f.setNbDeplacements(0);
        f.actualiser(p);
        f.reinitAide();
        f.majCoul(p.getNiveau());
        f.setNiveau(p.getDifficulte());
        defiComplet();
    }

    public void setAide(boolean aide) {
        this.aide = aide;
    }
    
    /**
     * Ouvre la page des regles
     */
    public void ouvrirAide() {
        Runtime rt = Runtime.getRuntime();
        String url = FenetreAccueil.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "ressources/Aide.html";
        String os = System.getProperty("os.name").toLowerCase();
        if(os.contains("win")) {
            try {
                rt.exec("rundll32 url.dll,FileProtocolHandler " + url.substring(1));
            } catch (IOException ignored) {}
        }else if(os.contains("nix") || os.contains("nux")) {
            String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
                    "netscape","opera","links","lynx"};

            StringBuffer cmd = new StringBuffer();
            for (int i=0; i<browsers.length; i++)
                cmd.append( (i==0  ? "" : " || " ) + browsers[i] +" \"" + url + "\" ");

            try {
                rt.exec(new String[]{"sh", "-c", cmd.toString()});
            } catch (IOException ignored) {}
        }
    }

    /**
     * Gere le Ctrl-Z
     */
    public void retour() {
        p.setNbDeplacements(p.getNbDeplacements()+1);
        f.setNbDeplacements(p.getNbDeplacements());
        p.retour();
    }

    /**
     * Definit un nouveau theme
     */
    public void setTheme(Icon imgRoi, Icon imgReine, Icon imgTour, Icon imgCavalier, Icon imgFou, Icon imgPion, Color coul) {
        this.imgRoi = imgRoi;
        this.imgReine = imgReine;
        this.imgTour = imgTour;
        this.imgCavalier = imgCavalier;
        this.imgFou = imgFou;
        this.imgPion = imgPion;
        this.coul = coul;
        f.dispose();
        jouer(false);
    }

    /**
     * Acceder aux differents elements du theme
     */
    public Icon getImgRoi() { return this.imgRoi; }
    public Icon getImgDame() { return this.imgReine; }
    public Icon getImgTour() { return this.imgTour; }
    public Icon getImgCavalier() { return this.imgCavalier; }
    public Icon getImgFou() { return this.imgFou; }
    public Icon getImgPion() { return this.imgPion; }
    public Color getCoul() { return this.coul; }

    /**
     * Ouvre une nouvelle fenetre de choix de niveau
     */
    public void choisirNiveau() {
        p.choixNiveau(this);
    }

    /**
     * Resout un niveau
     */
    public void resol() {
        p.setNbDeplacements(p.getNbDeplacements()+1);
        f.setNbDeplacements(p.getNbDeplacements());
        p.resol();
    }

    /**
     * Gere le deplacement des pieces
     * (le choix de la piece de depart et d'arrivee)
     * @param x,y les coordonnees de la piece
     */
    public void deplacer(int x, int y) {
        if(!f.getModeSelection()) { //Si on choisi la piece d'arrivee (deuxieme clic)
            if(x == posDepart.getX() && y == posDepart.getY())
                return;

            for (Position pos : alPos) {
                if(pos.getX() == x && pos.getY() == y && f.contientPiece(x, y))
                    p.deplacer(posDepart, new Position(x, y));
            }
        } else { //Si on choisi la piece de depart (premier clic)
            if(aide) { //Si l'aide est activee
                alPos = p.getTabPieces()[x][y].setDeplacement();
                for (Position pos : alPos) {
                    if (pos.getX() < p.getTaille() && pos.getX() >= 0 && pos.getY() < p.getTaille() && pos.getY() >= 0) {
                        if (!(p.getTabPieces()[pos.getX()][pos.getY()] != null && p.getTabPieces()[pos.getX()][pos.getY()].getType().equals("Roi")))
                            f.setCouleurPossibleVide(pos.getX(), pos.getY());
                    }
                }
            }
            alPos = p.getTabPieces()[x][y].updateDeplacement(p.getTabPieces());
            posDepart = new Position(x, y);
            if(aide) { //Si l'aide est activee
                for (Position pos : alPos) {
                    if (pos.getX() < p.getTaille() && pos.getX() >= 0 && pos.getY() < p.getTaille() && pos.getY() >= 0) {
                        if (!(p.getTabPieces()[pos.getX()][pos.getY()] != null && p.getTabPieces()[pos.getX()][pos.getY()].getType().equals("Roi")))
                            f.setCouleurPossible(pos.getX(), pos.getY());
                    }
                }
            }
            f.setCouleurDepart(x, y);
        }
    }

    /**
     * Definit le niveau actuel en "completé"
     * (ajoute une etoile a la fin de la ligne)
     */
    public static void defiComplet() {
        try {
            BufferedReader file = new BufferedReader(new FileReader("./ressources/Niveaux.txt"));
            String line;
            String input = "";
            int cpt = 1;
            while ((line = file.readLine()) != null) {
                if(input.isEmpty()) {
                    if(cpt == p.getNiveau())
                        input += line.replace("/", "*");
                    else
                        input += line;
                } else if(cpt == p.getNiveau()) {
                    input += '\n' + line.replace("/", "*");
                } else {
                    input += '\n' + line;
                }

                cpt++;
            }

            file.close();

            FileOutputStream fileOut = new FileOutputStream("./ressources/Niveaux.txt");
            fileOut.write(input.getBytes());
            fileOut.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(f, "Erreur lors de la sauvegarde",
                    "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Reinitialise la progression dans le fichier de niveaux
     */
    public static void reinitProgression() {
        try {
            BufferedReader file = new BufferedReader(new FileReader("./ressources/Niveaux.txt"));
            String line;
            String input = "";
            while ((line = file.readLine()) != null) {
                if(input.isEmpty()) {
                    input += line.replace("*", "/");
                } else {
                    input += '\n' + line.replace("*", "/");
                }
            }

            file.close();

            FileOutputStream fileOut = new FileOutputStream("./ressources/Niveaux.txt");
            fileOut.write(input.getBytes());
            fileOut.close();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(f, "Erreur lors de la réinitialisation",
                    "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void main(String arg[]) {
        new Controleur();
    }

}
