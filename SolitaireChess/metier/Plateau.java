package SolitaireChess.metier;

import SolitaireChess.Controleur;
import SolitaireChess.ihm.FenetreChoixNiveau;
import SolitaireChess.ihm.FenetreJeu;
import SolitaireChess.piece.*;

import javax.swing.*;
import java.awt.*;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Plateau {

    private	Piece[][] tabPieces = new Piece[4][4];
    private FenetreJeu f;
    private int niveau = 1;
    private int nbDeplacements = 0;
    private ArrayList<Piece[][]> alHistorique = new ArrayList<Piece[][]>();

    /**
     * Cree un nouveau Plateau
     * @param f la Fenetre de jeu associée
     */
    public Plateau(FenetreJeu f) {
        this.f = f;
        initPlateau(niveau);

    }

    /**
     * @return le tableau de pieces
     */
    public Piece[][] getTabPieces() {
        return tabPieces;
    }

    /**
     * @return la taille du tableau de pieces
     */
    public int getTaille() { return tabPieces.length; }

    /**
     * @return le nombre e deplacements
     */
    public int getNbDeplacements() { return this.nbDeplacements; }

    /**
     * @return le niveau
     */
    public int getNiveau() { return this.niveau; }

    /**
     *
     * @return
     */
    public String getDifficulte() {
        if((double)(niveau)/15 <= 1)
            return "(niv " + niveau + " - Débutant)";
        else if((double)(niveau)/15 <= 2)
            return "(niv " + niveau + " - Intermédiaire)";
        else if((double)(niveau)/15 <= 3)
            return "(niv " + niveau + " - Avancé)";
        else if((double)(niveau)/15 <= 4)
            return "(niv " + niveau + " - Expert)";
        else
            return "(niv " + niveau + " - Personnalisé)";
    }

    /**
     * @param nbDeplacements definit le nombre de deplacements
     */
    public void setNbDeplacements(int nbDeplacements) {
        this.nbDeplacements = nbDeplacements;
    }

    /**
     * Ouvre une fenetre de choix de niveau
     * @param c le controleur actuel
     */
    public void choixNiveau(Controleur c) {
        new FenetreChoixNiveau(this, c);
    }

    /**
     * Initialise la position des pieces depuis les fichiers
     * @param niv le niveau a charger
     */
    public void initPlateau(int niv) {
        this.alHistorique.clear();
        try {

            //On efface le tableau de pieces
            for (int ligne = 0;ligne < tabPieces.length; ligne++)
                for (int colonne = 0; colonne < tabPieces[ligne].length; colonne++)
                    tabPieces[ligne][colonne] = null;

			Scanner sc;

            int numLigne;

            //On verifie si on doit chercher dans les niveaux personnalises (niv > 60)
            this.niveau = niv;
            if(niv > 60) {
                sc = new Scanner(new FileReader("./ressources/NiveauxPerso.txt"));
                numLigne = this.niveau-60;
            } else {
                sc = new Scanner(new FileReader("./ressources/Niveaux.txt"));
                numLigne = this.niveau;
            }

            int cpt = 1;

            //On parcoure le fichier
			while(sc.hasNextLine() && ((this.niveau <=60 && cpt <= 60)||(this.niveau>60 && cpt <=15))) {
                String ligne = sc.next();
                
                if(cpt == numLigne){

                    String[] s = ligne.split("(?<=\\G.{3})");

                    for(String str : s){
                        if(!str.contains("/") & !str.contains("*")) {
                            int l = Character.getNumericValue(str.charAt(1));
                            int c = Character.getNumericValue(str.charAt(2));
                            switch (str.charAt(0)) {
                                case 'P':
                                    tabPieces[l][c] = new Pion(l, c);
                                    break;
                                case 'T':
                                    tabPieces[l][c] = new Tour(l, c);
                                    break;
                                case 'R':
                                    tabPieces[l][c] = new Roi(l, c);
                                    break;
                                case 'D':
                                    tabPieces[l][c] = new Dame(l, c);
                                    break;
                                case 'F':
                                    tabPieces[l][c] = new Fou(l, c);
                                    break;
                                case 'C':
                                    tabPieces[l][c] = new Cavalier(l, c);
                                    break;
                            }
                        }
                    }

                }
                cpt++;
            }

            //Gere la fin des niveaux
            for(int i = 0; i < tabPieces.length; i++)
                for(int j = 0; j < tabPieces[i].length; j++)
                    if(tabPieces[i][j] != null)
                        return;
            if(this.niveau > 60)
                JOptionPane.showMessageDialog(null, "Vous avez fini les niveaux personnalisés, veuillez charger d'autres niveaux");
            else
                JOptionPane.showMessageDialog(null, "Vous avez fini les niveaux normaux, veuillez charger d'autres niveaux");

		}catch(IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Gere le bouton retour (Ctrl-Z)
     */
    public void retour() {
        if (alHistorique.size() > 0) {
            this.tabPieces = alHistorique.get(alHistorique.size() - 1);
            alHistorique.remove(alHistorique.size() - 1);
        }
        f.reinitAide();
        f.actualiser(this);
    }

    /**
     * Deplace une piece
     * @param posDepart la position du pion de depart
     * @param posArrive la position du pion d'arrivée
     */
    public void deplacer(Position posDepart, Position posArrive) {

        if(tabPieces[posArrive.getX()][posArrive.getY()].getType().equals("Roi"))
            return;

        this.nbDeplacements++;
        Piece[][] p = new Piece[4][4];
        for (int ligne = 0;ligne < tabPieces.length; ligne++)
            for (int colonne = 0; colonne < tabPieces[ligne].length; colonne++) {
                if(tabPieces[ligne][colonne] != null) {
                    switch (tabPieces[ligne][colonne].getType().charAt(0)) {
                        case 'P':
                            p[ligne][colonne] = new Pion(ligne, colonne);
                            break;
                        case 'T':
                            p[ligne][colonne] = new Tour(ligne, colonne);
                            break;
                        case 'R':
                            p[ligne][colonne] = new Roi(ligne, colonne);
                            break;
                        case 'D':
                            p[ligne][colonne] = new Dame(ligne, colonne);
                            break;
                        case 'F':
                            p[ligne][colonne] = new Fou(ligne, colonne);
                            break;
                        case 'C':
                            p[ligne][colonne] = new Cavalier(ligne, colonne);
                            break;
                    }
                }
            }

        this.alHistorique.add(p);

        tabPieces[posArrive.getX()][posArrive.getY()] = tabPieces[posDepart.getX()][posDepart.getY()];
        tabPieces[posArrive.getX()][posArrive.getY()].setPos(posArrive.getX(), posArrive.getY());
        tabPieces[posDepart.getX()][posDepart.getY()] = null;


        f.actualiser(this);
        f.setNbDeplacements(this.nbDeplacements);

        if(verifGagne()) {
            alHistorique.clear();
            f.gagne();
        }
    }

    /**
     * Gere la resolution d'un deplacement
     */
    public void resol()
    {
        if (getNiveau() > 15)
        {
            JOptionPane.showMessageDialog(f, "Desolé, les aides ne sont disponibles que pour les defis DEBUTANT.",
                    "Aie !", JOptionPane.INFORMATION_MESSAGE);
        }

        int numTour = this.alHistorique.size();
        try
        {
            Scanner sc;
            sc = new Scanner(new FileReader("./ressources/Resolutions.txt"));

            int cpt = 1;
            int lvl = getNiveau();
            char c1;
            char c2;
            boolean help = false;

            while(sc.hasNextLine() && cpt<=lvl && cpt <= 60)
            {
                String ligne = sc.next();
                if(cpt==lvl)
                {
                    String[] s = ligne.split("(?<=\\G.{6})");
                    if(getTabPieces()[((int)s[numTour].charAt(1))-48] [((int)s[numTour].charAt(2))-48] != null)
                    {
                        c1 = getTabPieces()[((int)s[numTour].charAt(1))-48] [((int)s[numTour].charAt(2))-48].getType().charAt(0);

                        if(getTabPieces()[((int)s[numTour].charAt(4))-48] [((int)s[numTour].charAt(5))-48] != null)
                        {
                            c2 = getTabPieces()[((int)s[numTour].charAt(4))-48] [((int)s[numTour].charAt(5))-48].getType().charAt(0);

                            if((s[numTour].charAt(0) == c1) && (s[numTour].charAt(3) == c2))
                            {
                                //La couleur des cases a deplacer
                                f.setCouleurDepart(((int)s[numTour].charAt(1))-48, ((int)s[numTour].charAt(2))-48);
                                f.setCouleurPossible(((int)s[numTour].charAt(4))-48, ((int)s[numTour].charAt(5))-48);

                                help = true;

                            }
                        }
                    }

                    if(!help)
                    {
                        JOptionPane.showMessageDialog(f, "Desolé ! Aucun déplacement autorisé ne pourra vous faire gagner ce défi. \n" +
                                        " Veuillez annuler votre dernier coup ou recommencer le défi",
                                "Aie !", JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                cpt++;
            }
        }
        catch(IOException e) {System.out.println(e);}
    }

    /**
     * @return si le defi est fini
     */
    private boolean verifGagne() {
        int cpt = 0;

        //On verfie si il reste UNE SEULE piece
        for (int ligne = 0;ligne < tabPieces.length; ligne++)
            for (int colonne = 0; colonne < tabPieces[ligne].length; colonne++)
                if(tabPieces[ligne][colonne] != null) cpt++;

        if(cpt > 1) {
            return false;
        }
        else
            for (int ligne = 0;ligne < tabPieces.length; ligne++)
                for (int colonne = 0; colonne < tabPieces[ligne].length; colonne++)
                    tabPieces[ligne][colonne] = null;
            return true;
    }
    
}
