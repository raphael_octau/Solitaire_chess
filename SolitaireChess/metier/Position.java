package SolitaireChess.metier;

public class Position
{
	int x;
	int y;

	/**
	 * Cree une nouvelle position avec des coordonnees
	 * @param x,y les positions
     */
	public Position(int x, int y)
	{
		this.x=x;
		this.y=y;
	}

	public int getX(){return this.x;}
	public int getY(){return this.y;}

	public String toString(){return "["+this.x+";"+this.y+"]";}

}