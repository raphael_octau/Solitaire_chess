package SolitaireChess.ihm;

import SolitaireChess.Controleur;
import SolitaireChess.piece.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;

public class FenetreCreerNiveau extends JFrame implements ActionListener {

    private JButton listeButton[][] = new JButton[4][4];
    private Piece[][] pieces = new Piece[4][4];
    private ArrayList<JButton> alButtonPieces = new ArrayList<JButton>();
    private Controleur c;
    private JButton bRoi, bReine, bTour, bCavalier, bFou, bPion;
    private JPanel grille;
    private boolean bSelect = false;
    private Icon piece;

    public FenetreCreerNiveau(Controleur c) {
        this.c = c;

        setLocation(100, 100);
        setResizable(false);
        setTitle("Créer un niveau");
        setIconImage(new ImageIcon("./ressources/Images/cavalier.gif").getImage());

        JPanel jCreation = new JPanel(new GridBagLayout());


        JPanel jListe = new JPanel(new GridLayout(6, 1, 8, 8));
        jListe.setBorder(new EmptyBorder(20, 20, 20, 20));

        bRoi = new JButton(c.getImgRoi());
        bRoi.setPreferredSize(new Dimension(60, 60));
        bRoi.setFocusPainted(false);
        bRoi.setBorder(null);
        bRoi.setBackground(Color.LIGHT_GRAY);
        bRoi.addActionListener(this);

        bReine = new JButton(c.getImgDame());
        bReine.setPreferredSize(new Dimension(60, 60));
        bReine.setFocusPainted(false);
        bReine.setBorder(null);
        bReine.setBackground(Color.LIGHT_GRAY);
        bReine.addActionListener(this);

        bTour = new JButton(c.getImgTour());
        bTour.setPreferredSize(new Dimension(60, 60));
        bTour.setFocusPainted(false);
        bTour.setBorder(null);
        bTour.setBackground(Color.LIGHT_GRAY);
        bTour.addActionListener(this);

        bCavalier = new JButton(c.getImgCavalier());
        bCavalier.setPreferredSize(new Dimension(60, 60));
        bCavalier.setFocusPainted(false);
        bCavalier.setBorder(null);
        bCavalier.setBackground(Color.LIGHT_GRAY);
        bCavalier.addActionListener(this);

        bFou = new JButton(c.getImgFou());
        bFou.setPreferredSize(new Dimension(60, 60));
        bFou.setFocusPainted(false);
        bFou.setBorder(null);
        bFou.setBackground(Color.LIGHT_GRAY);
        bFou.addActionListener(this);

        bPion = new JButton(c.getImgPion());
        bPion.setPreferredSize(new Dimension(60, 60));
        bPion.setFocusPainted(false);
        bPion.setBorder(null);
        bPion.setBackground(Color.LIGHT_GRAY);
        bPion.addActionListener(this);

        alButtonPieces.add(bRoi);
        alButtonPieces.add(bReine);
        alButtonPieces.add(bTour);
        alButtonPieces.add(bCavalier);
        alButtonPieces.add(bFou);
        alButtonPieces.add(bPion);

        jListe.add(bRoi);
        jListe.add(bReine);
        jListe.add(bTour);
        jListe.add(bCavalier);
        jListe.add(bFou);
        jListe.add(bPion);

        jCreation.add(jListe);

        //---------- GRILLE ----------//
        JPanel ensGrille = new JPanel(new BorderLayout());

        grille = new JPanel(new GridLayout(4, 4));
        grille.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(20, 20, 20, 20), new LineBorder(Color.DARK_GRAY, 4)));

        for(int i=0; i<listeButton.length; i++){
            for(int j=0; j<listeButton.length; j++){
                listeButton[i][j]= new JButton();
                listeButton[i][j].setPreferredSize(new Dimension(100, 100));
                listeButton[i][j].setBorderPainted( false );
                listeButton[i][j].setFocusPainted( false );

                final int iFinal = i, jFinal = j;

                listeButton[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        JButton b = (JButton)actionEvent.getSource();
                        b.setIcon(piece);

                        if(piece.toString().toLowerCase().contains("roi"))
                            pieces[iFinal][jFinal] = new Roi(iFinal, jFinal);
                        if(piece.toString().toLowerCase().contains("reine"))
                            pieces[iFinal][jFinal] = new Dame(iFinal, jFinal);
                        if(piece.toString().toLowerCase().contains("tour"))
                            pieces[iFinal][jFinal] = new Tour(iFinal, jFinal);
                        if(piece.toString().toLowerCase().contains("cavalier"))
                            pieces[iFinal][jFinal] = new Cavalier(iFinal, jFinal);
                        if(piece.toString().toLowerCase().contains("fou"))
                            pieces[iFinal][jFinal] = new Fou(iFinal, jFinal);
                        if(piece.toString().toLowerCase().contains("pion"))
                            pieces[iFinal][jFinal] = new Pion(iFinal, jFinal);
                    }
                });

                if ((i + j) % 2 == 0) listeButton[i][j].setBackground(Color.WHITE);
                else listeButton[i][j].setBackground(Color.LIGHT_GRAY);

                grille.add(listeButton[i][j]);
            }
        }
        ensGrille.add(grille);

        jCreation.add(ensGrille);

        add(jCreation);

        JPanel pBoutons = new JPanel(new FlowLayout());

        JButton bValider = new JButton("Valider");
        bValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int res = JOptionPane.showConfirmDialog(null,
                        "Assurez-vous que le niveau soit réalisable avant de le sauvegarder.",
                        "Avertissement",
                        JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (res == JOptionPane.YES_OPTION) {
                    creerNiveau();
                }else if (res == JOptionPane.NO_OPTION){
                    return;
                }
            }
        });
        pBoutons.add(bValider);

        JButton bReinit = new JButton("Réinitialiser la grille");
        bReinit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                pieces = new Piece[4][4];
                for(JButton[] p : listeButton)
                    for(JButton p2 : p)
                        p2.setIcon(null);
            }
        });
        pBoutons.add(bReinit);

        JButton bEffacer = new JButton("Effacer la liste des niveaux");
        bEffacer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int res = JOptionPane.showConfirmDialog(null,
                        "Etes-vous sur de vouloir supprimer la totalité des niveaux personnalisés ?",
                        "Avertissement",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE);
                if (res == JOptionPane.YES_OPTION) {
                    try {
                        PrintWriter writer = new PrintWriter("./ressources/NiveauxPerso.txt");
                        writer.print("");
                        writer.close();
                        JOptionPane.showMessageDialog(null, "Les niveaux ont bien étés supprimés");
                    } catch (IOException e) {
                        System.out.println("Erreur lors de la suppression des niveaux personnalisés");
                    }
                }else if (res == JOptionPane.NO_OPTION){
                    return;
                }
            }
        });
        pBoutons.add(bEffacer);

        add(pBoutons, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        bSelect = !bSelect;
        JButton b = (JButton)e.getSource();
        if(b.getIcon() != piece)
            bSelect = true;
        if(bSelect) {
            grille.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(20, 20, 20, 20), new LineBorder(Color.ORANGE, 4)));
            for(JButton btn : alButtonPieces)
                btn.setBorder(null);

            b.setBorder(new LineBorder(Color.ORANGE, 2));
            piece = b.getIcon();
        } else {
            grille.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(20, 20, 20, 20), new LineBorder(Color.DARK_GRAY, 4)));
            for(JButton btn : alButtonPieces)
                btn.setBorder(null);

            piece = null;
        }
    }

    private void creerNiveau() {
        boolean bPremiereLigne = false;
        int nbPieces = 0;

        for(Piece[] p : pieces)
            for(Piece p2 : p)
                if(p2 != null)
                    nbPieces++;

        if(nbPieces < 2) {
            JOptionPane.showMessageDialog(this, "Vous devez créer un niveau valide avant de l'enregistrer");
            return;
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader("./ressources/NiveauxPerso.txt"));
            int lines = 0;
            while (reader.readLine() != null) lines++;
            reader.close();

            if(lines > 15) {
                JOptionPane.showMessageDialog(this, "Impossible de créer un nouveau niveau, limite dépassée.");
                return;
            } else if(lines == 0) {
                bPremiereLigne = true;
            }

            String s = "";

            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("./ressources/NiveauxPerso.txt", true)));

            for(int i = 0; i < pieces.length; i++) {
                for(int j = 0; j < pieces[i].length; j++) {
                    if(pieces[i][j] != null) {
                        if(pieces[i][j].getType().equals("Roi"))
                            s += "R" + i + j;
                        if(pieces[i][j].getType().equals("Dame"))
                            s += "D" + i + j;
                        if(pieces[i][j].getType().equals("Tour"))
                            s += "T" + i + j;
                        if(pieces[i][j].getType().equals("Cavalier"))
                            s += "C" + i + j;
                        if(pieces[i][j].getType().equals("Fou"))
                            s += "F" + i + j;
                        if(pieces[i][j].getType().equals("Pion"))
                            s += "P" + i + j;
                    }
                }
            }
            if(bPremiereLigne)
                out.print(s + "/");
            else
                out.print("\n" + s + "/");
            out.close();
            JOptionPane.showMessageDialog(this, "Sauvegarde réussie, " + ((15-lines)-1) + " niveaux restants");
        } catch (IOException e) {
            System.out.println("Erreur lors de la sauvegarde du niveau");
        }
    }
}
