package SolitaireChess.ihm;

import SolitaireChess.Controleur;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class FenetreAccueil extends JFrame implements ActionListener {

    private Controleur c;
    private JButton bNouv, bCreer, bCharger, bQuitter, bRegles;

    /**
     * Cree une nouvelle Fenetre d'accueil
     * @param c le Controlleur actuel
     */
    public FenetreAccueil(Controleur c) {
        this.c = c;

        Color coul;
        if (c.getCoul() != null)
            coul = c.getCoul();
        else
            coul = new Color(174,224,167);

        setLocation(100, 100);
        setResizable(false);
        setTitle("Accueil - Solitaire Chess");
        setIconImage(new ImageIcon("./ressources/Images/cavalier.gif").getImage());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel j = new JPanel();
        j.setLayout(new GridLayout(3, 3));
        j.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(20, 20, 20, 20), new LineBorder(Color.DARK_GRAY, 2)));

        JLabel lImage = new JLabel(new ImageIcon("./ressources/Images/cavalier.gif"));
        lImage.setFont(new Font(lImage.getName(), Font.BOLD, 30));
        lImage.setText("Solitaire Chess");
        lImage.setHorizontalTextPosition(JLabel.CENTER);
        lImage.setVerticalTextPosition(JLabel.BOTTOM);
        lImage.setAlignmentX(Component.CENTER_ALIGNMENT);
        lImage.setBorder(new EmptyBorder(10, 10, 10, 10));

        add(lImage);

        bNouv = new JButton("<html><center>Nouvelle<br />partie</center></html>");
        bNouv.setAlignmentX(Component.CENTER_ALIGNMENT);
        bNouv.addActionListener(this);
        bNouv.setFocusPainted(false);
        bNouv.setBorder(null);
        bNouv.setFont(new Font(lImage.getName(), Font.BOLD, 15));
        bNouv.setBackground(coul);
        bNouv.setPreferredSize(new Dimension(100, 100));

        bCreer = new JButton("<html><center>Créer<br />un défi</center></html>");
        bCreer.setAlignmentX(Component.CENTER_ALIGNMENT);
        bCreer.addActionListener(this);
        bCreer.setFocusPainted(false);
        bCreer.setBorder(null);
        bCreer.setFont(new Font(lImage.getName(), Font.BOLD, 15));
        bCreer.setBackground(coul);
        bCreer.setPreferredSize(new Dimension(100, 100));

        bCharger = new JButton("<html><center>Charger<br />une partie</center></html>");
        bCharger.setAlignmentX(Component.CENTER_ALIGNMENT);
        bCharger.addActionListener(this);
        bCharger.setFocusPainted(false);
        bCharger.setBorder(null);
        bCharger.setFont(new Font(lImage.getName(), Font.BOLD, 15));
        bCharger.setBackground(coul);
        bCharger.setPreferredSize(new Dimension(100, 100));

        bRegles = new JButton("Règles");
        bRegles.setAlignmentX(Component.CENTER_ALIGNMENT);
        bRegles.addActionListener(this);
        bRegles.setFocusPainted(false);
        bRegles.setBorder(null);
        bRegles.setFont(new Font(lImage.getName(), Font.BOLD, 15));
        bRegles.setBackground(coul);
        bRegles.setPreferredSize(new Dimension(100, 100));

        bQuitter = new JButton("Quitter");
        bQuitter.setAlignmentX(Component.CENTER_ALIGNMENT);
        bQuitter.addActionListener(this);
        bQuitter.setFocusPainted(false);
        bQuitter.setBorder(null);
        bQuitter.setFont(new Font(lImage.getName(), Font.BOLD, 15));
        bQuitter.setBackground(coul);
        bQuitter.setPreferredSize(new Dimension(100, 100));

        j.add(bNouv);
        j.add(new JLabel());
        j.add(bCharger);
        j.add(new JLabel());
        j.add(bCreer);
        j.add(new JLabel());
        j.add(bRegles);
        j.add(new JLabel());
        j.add(bQuitter);

        add(j, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Ecoute les actions sur les boutons de la fenetre
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == bQuitter) {
            System.exit(0);
        }
        if(e.getSource() == bNouv) {
            int res = JOptionPane.showConfirmDialog(this,
                    "Etes-vous sûr de vouloir créer une nouvelle partie?\nToute progression sera perdue.",
                    "Nouvelle partie",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (res == JOptionPane.YES_OPTION) {
                c.jouer(false);
                c.nouvellePartie();
                this.dispose();
            }else if (res == JOptionPane.NO_OPTION){
                return;
            }
        }
        if(e.getSource() == bCharger) {
            c.jouer(true);
            this.dispose();
        }
        if(e.getSource() == bCreer) {
            new FenetreCreerNiveau(c);
        }
        if(e.getSource() == bRegles) {
            c.ouvrirAide();
        }
    }

}
