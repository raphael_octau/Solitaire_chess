package SolitaireChess.ihm;

import SolitaireChess.Controleur;
import SolitaireChess.piece.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;

public class FenetreTheme extends JFrame implements ItemListener, ActionListener {

    private Controleur c;

    private JPanel grille;
    private JButton listeButton[][] = new JButton[4][4];
    private Piece listePieces[][] = new Piece[4][4];
    private JLabel lRoi, lReine, lTour, lCavalier, lFou, lPion;
    private JButton bRoi, bReine, bTour, bCavalier, bFou, bPion, bAppliquer, bCouleur;
    private JPanel pPrevisu;
    private JComboBox comboBox;
    private String[] listeThemes = new String[] {"Classique", "Moderne", "Personnalisé"};

    /**
     * Cree une nouvelle Fenetre pour choisir un theme (images et couleurs)
     * @param c le controleur actuel
     */
    public FenetreTheme(Controleur c) {
        this.c = c;

        setLocation(100, 100);
        setResizable(false);
        setTitle("Choisir un thème");
        setIconImage(new ImageIcon("./ressources/Images/cavalier.gif").getImage());

        //---------- LISTE THEMES ----------//
        JPanel jTheme = new JPanel(new FlowLayout());
        comboBox = new JComboBox(listeThemes);
        comboBox.addItemListener(this);
        JLabel l = new JLabel("Choisissez votre thème : ");
        jTheme.add(l);
        jTheme.add(comboBox);
        jTheme.setBackground(Color.LIGHT_GRAY);

        add(jTheme, BorderLayout.NORTH);


        JPanel corps = new JPanel(new GridBagLayout());

        //---------- OPTIONS ----------//
        JPanel jListe = new JPanel(new GridLayout(7, 1));
        jListe.setBorder(new EmptyBorder(10, 10, 10, 10));

        JPanel p = new JPanel(new FlowLayout());
        lRoi = new JLabel(redimensionner(new ImageIcon("./ressources/Images/roi.gif")));
        lRoi.setPreferredSize(new Dimension(50, 70));
        lRoi.setText("Roi");
        lRoi.setHorizontalTextPosition(JLabel.CENTER);
        lRoi.setVerticalTextPosition(JLabel.BOTTOM);
        lRoi.setAlignmentX(Component.CENTER_ALIGNMENT);
        bRoi = new JButton("Choissir un fichier ...");
        bRoi.addActionListener(this);
        p.add(lRoi);
        p.add(bRoi);
        jListe.add(p);

        p = new JPanel(new FlowLayout());
        lReine = new JLabel(redimensionner(new ImageIcon("./ressources/Images/reine.gif")));
        lReine.setPreferredSize(new Dimension(50, 70));
        lReine.setText("Dame");
        lReine.setHorizontalTextPosition(JLabel.CENTER);
        lReine.setVerticalTextPosition(JLabel.BOTTOM);
        lReine.setAlignmentX(Component.CENTER_ALIGNMENT);
        bReine = new JButton("Choissir un fichier ...");
        bReine.addActionListener(this);
        p.add(lReine);
        p.add(bReine);
        jListe.add(p);

        p = new JPanel(new FlowLayout());
        lTour = new JLabel(redimensionner(new ImageIcon("./ressources/Images/tour.gif")));
        lTour.setPreferredSize(new Dimension(50, 70));
        lTour.setText("Tour");
        lTour.setHorizontalTextPosition(JLabel.CENTER);
        lTour.setVerticalTextPosition(JLabel.BOTTOM);
        lTour.setAlignmentX(Component.CENTER_ALIGNMENT);
        bTour = new JButton("Choissir un fichier ...");
        bTour.addActionListener(this);
        p.add(lTour);
        p.add(bTour);
        jListe.add(p);

        p = new JPanel(new FlowLayout());
        lCavalier = new JLabel(redimensionner(new ImageIcon("./ressources/Images/cavalier.gif")));
        lCavalier.setPreferredSize(new Dimension(50, 70));
        lCavalier.setText("Cavalier");
        lCavalier.setHorizontalTextPosition(JLabel.CENTER);
        lCavalier.setVerticalTextPosition(JLabel.BOTTOM);
        lCavalier.setAlignmentX(Component.CENTER_ALIGNMENT);
        bCavalier = new JButton("Choissir un fichier ...");
        bCavalier.addActionListener(this);
        p.add(lCavalier);
        p.add(bCavalier);
        jListe.add(p);

        p = new JPanel(new FlowLayout());
        lFou = new JLabel(redimensionner(new ImageIcon("./ressources/Images/fou.gif")));
        lFou.setPreferredSize(new Dimension(50, 70));
        lFou.setText("Fou");
        lFou.setHorizontalTextPosition(JLabel.CENTER);
        lFou.setVerticalTextPosition(JLabel.BOTTOM);
        lFou.setAlignmentX(Component.CENTER_ALIGNMENT);
        bFou = new JButton("Choissir un fichier ...");
        bFou.addActionListener(this);
        p.add(lFou);
        p.add(bFou);
        jListe.add(p);

        p = new JPanel(new FlowLayout());
        lPion = new JLabel(redimensionner(new ImageIcon("./ressources/Images/pion.gif")));
        lPion.setPreferredSize(new Dimension(50, 70));
        lPion.setText("Pion");
        lPion.setHorizontalTextPosition(JLabel.CENTER);
        lPion.setVerticalTextPosition(JLabel.BOTTOM);
        lPion.setAlignmentX(Component.CENTER_ALIGNMENT);
        bPion = new JButton("Choissir un fichier ...");
        bPion.addActionListener(this);
        p.add(lPion);
        p.add(bPion);
        jListe.add(p);

        p = new JPanel(new FlowLayout());
        pPrevisu = new JPanel();
        pPrevisu.setPreferredSize(new Dimension(50, 50));
        pPrevisu.setBackground(Color.LIGHT_GRAY);
        bCouleur = new JButton("Choissir une couleur ...");
        bCouleur.addActionListener(this);
        p.add(pPrevisu);
        p.add(bCouleur);

        bRoi.setEnabled(false);
        bReine.setEnabled(false);
        bTour.setEnabled(false);
        bCavalier.setEnabled(false);
        bFou.setEnabled(false);
        bPion.setEnabled(false);
        jListe.add(p);

        corps.add(jListe);

        //---------- GRILLE ----------//
        JPanel ensGrille = new JPanel(new BorderLayout());

        grille = new JPanel(new GridLayout(4, 4));
        grille.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(20, 20, 20, 20), new LineBorder(Color.DARK_GRAY, 4)));

        for(int i=0; i<listeButton.length; i++){
            for(int j=0; j<listeButton.length; j++){
                listeButton[i][j]= new JButton();
                listeButton[i][j].setPreferredSize(new Dimension(100, 100));
                listeButton[i][j].setBorderPainted( false );
                listeButton[i][j].setFocusPainted( false );

                if ((i + j) % 2 == 0) listeButton[i][j].setBackground(Color.WHITE);
                else listeButton[i][j].setBackground(Color.LIGHT_GRAY);

                grille.add(listeButton[i][j]);
            }
        }

        listePieces[1][1] = new Roi(1, 1);
        listeButton[1][1].setIcon(lRoi.getIcon());

        listePieces[2][0] = new Dame(1, 1);
        listeButton[2][0].setIcon(lReine.getIcon());

        listePieces[3][2] = new Tour(1, 1);
        listeButton[3][2].setIcon(lTour.getIcon());

        listePieces[0][3] = new Cavalier(1, 1);
        listeButton[0][3].setIcon(lCavalier.getIcon());

        listePieces[0][1] = new Fou(1, 1);
        listeButton[0][1].setIcon(lFou.getIcon());

        listePieces[3][0] = new Pion(1, 1);
        listeButton[3][0].setIcon(lPion.getIcon());

        ensGrille.add(grille);

        corps.add(grille);

        //---------- VALIDER ----------//
        bAppliquer = new JButton("Appliquer");
        bAppliquer.addActionListener(this);
        bAppliquer.setPreferredSize(new Dimension(10, 40));
        add(bAppliquer, BorderLayout.SOUTH);

        add(corps);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Choisit une image avec une fenetre de dialogue
     * @return l'image choisie
     */
    private ImageIcon chargerImage() {
        JFileChooser fc = new JFileChooser();
        fc.setCurrentDirectory(new File("."));
        int retVal = fc.showOpenDialog(null);
        if (retVal == 0) {
            File file = fc.getSelectedFile();
            try {
                return new ImageIcon(ImageIO.read(file));
            } catch (IOException ignored) {
            }
        }
        return null;
    }

    /**
     * Redimensionne l'image pour s'adapter a la largeur ou la hauteur de la case
     * @param img l'image a redimensionner
     * @return l'image redimensionnee
     */
    private ImageIcon redimensionner(ImageIcon img) {
        Image image = img.getImage();
        Image newimg;
        if(img.getIconWidth() > img.getIconHeight())
            newimg = image.getScaledInstance(50, img.getIconHeight()/(img.getIconWidth()/50),  java.awt.Image.SCALE_SMOOTH);
        else
            newimg = image.getScaledInstance(img.getIconWidth()/(img.getIconHeight()/50), 50,  java.awt.Image.SCALE_SMOOTH);

        return new ImageIcon(newimg);
    }

    /**
     * Ecoute le changement de theme dans la liste deroulante
     */
    public void itemStateChanged(ItemEvent itemEvent) {
        if(itemEvent.getStateChange() == ItemEvent.SELECTED) {
            if (comboBox.getSelectedItem() == listeThemes[0]) {
                lRoi.setIcon(redimensionner(new ImageIcon("./ressources/Images/roi.gif")));
                lReine.setIcon(redimensionner(new ImageIcon("./ressources/Images/reine.gif")));
                lTour.setIcon(redimensionner(new ImageIcon("./ressources/Images/tour.gif")));
                lCavalier.setIcon(redimensionner(new ImageIcon("./ressources/Images/cavalier.gif")));
                lFou.setIcon(redimensionner(new ImageIcon("./ressources/Images/fou.gif")));
                lPion.setIcon(redimensionner(new ImageIcon("./ressources/Images/pion.gif")));
                for(int i = 0; i < listeButton.length; i++) {
                    for(int j = 0; j < listeButton[i].length; j++) {
                        if(listePieces[i][j] != null) {
                            switch (listePieces[i][j].getType().charAt(0)) {
                                case 'P':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/pion.gif")));
                                    break;
                                case 'T':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/tour.gif")));
                                    break;
                                case 'R':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/roi.gif")));
                                    break;
                                case 'D':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/reine.gif")));
                                    break;
                                case 'F':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/fou.gif")));
                                    break;
                                case 'C':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/cavalier.gif")));
                                    break;
                            }
                        }
                    }
                }
                bRoi.setEnabled(false);
                bReine.setEnabled(false);
                bTour.setEnabled(false);
                bCavalier.setEnabled(false);
                bFou.setEnabled(false);
                bPion.setEnabled(false);
            }
            if (comboBox.getSelectedItem() == listeThemes[1]) {
                lRoi.setIcon(redimensionner(new ImageIcon("./ressources/Images/roi.png")));
                lReine.setIcon(redimensionner(new ImageIcon("./ressources/Images/reine.png")));
                lTour.setIcon(redimensionner(new ImageIcon("./ressources/Images/tour.png")));
                lCavalier.setIcon(redimensionner(new ImageIcon("./ressources/Images/cavalier.png")));
                lFou.setIcon(redimensionner(new ImageIcon("./ressources/Images/fou.png")));
                lPion.setIcon(redimensionner(new ImageIcon("./ressources/Images/pion.png")));
                for(int i = 0; i < listeButton.length; i++) {
                    for(int j = 0; j < listeButton[i].length; j++) {
                        if(listePieces[i][j] != null) {
                            switch (listePieces[i][j].getType().charAt(0)) {
                                case 'P':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/pion.png")));
                                    break;
                                case 'T':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/tour.png")));
                                    break;
                                case 'R':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/roi.png")));
                                    break;
                                case 'D':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/reine.png")));
                                    break;
                                case 'F':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/fou.png")));
                                    break;
                                case 'C':
                                    listeButton[i][j].setIcon(redimensionner(new ImageIcon("./ressources/Images/cavalier.png")));
                                    break;
                            }
                        }
                    }
                }
                bRoi.setEnabled(false);
                bReine.setEnabled(false);
                bTour.setEnabled(false);
                bCavalier.setEnabled(false);
                bFou.setEnabled(false);
                bPion.setEnabled(false);
            }
            if (comboBox.getSelectedItem() == listeThemes[2]) {
                bRoi.setEnabled(true);
                bReine.setEnabled(true);
                bTour.setEnabled(true);
                bCavalier.setEnabled(true);
                bFou.setEnabled(true);
                bPion.setEnabled(true);
            }
        }

    }

    /**
     * Ecoute le clic sur les boutons
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == bAppliquer) {
            c.setTheme(lRoi.getIcon(), lReine.getIcon(), lTour.getIcon(), lCavalier.getIcon(), lFou.getIcon(), lPion.getIcon(), pPrevisu.getBackground());
        }
        if(e.getSource() == bRoi) {
            ImageIcon icon = chargerImage();
            lRoi.setIcon(redimensionner(icon));
            for(int i = 0; i < listeButton.length; i++) {
                for(int j = 0; j < listeButton[i].length; j++) {
                    if(listePieces[i][j] != null) {
                        if (listePieces[i][j].getType().equals("Roi"))
                            listeButton[i][j].setIcon(redimensionner(icon));
                    }
                }
            }
        }
        if(e.getSource() == bReine) {
            ImageIcon icon = chargerImage();
            lReine.setIcon(redimensionner(icon));
            for(int i = 0; i < listeButton.length; i++) {
                for(int j = 0; j < listeButton[i].length; j++) {
                    if(listePieces[i][j] != null) {
                        if (listePieces[i][j].getType().equals("Dame"))
                            listeButton[i][j].setIcon(redimensionner(icon));
                    }
                }
            }
        }
        if(e.getSource() == bTour) {
            ImageIcon icon = chargerImage();
            lTour.setIcon(redimensionner(icon));
            for(int i = 0; i < listeButton.length; i++) {
                for(int j = 0; j < listeButton[i].length; j++) {
                    if(listePieces[i][j] != null) {
                        if (listePieces[i][j].getType().equals("Tour"))
                            listeButton[i][j].setIcon(redimensionner(icon));
                    }
                }
            }
        }
        if(e.getSource() == bCavalier) {
            ImageIcon icon = chargerImage();
            lCavalier.setIcon(redimensionner(icon));
            for(int i = 0; i < listeButton.length; i++) {
                for(int j = 0; j < listeButton[i].length; j++) {
                    if(listePieces[i][j] != null) {
                        if (listePieces[i][j].getType().equals("Cavalier"))
                            listeButton[i][j].setIcon(redimensionner(icon));
                    }
                }
            }
        }
        if(e.getSource() == bFou) {
            ImageIcon icon = chargerImage();
            lFou.setIcon(redimensionner(icon));
            for(int i = 0; i < listeButton.length; i++) {
                for(int j = 0; j < listeButton[i].length; j++) {
                    if(listePieces[i][j] != null) {
                        if (listePieces[i][j].getType().equals("Fou"))
                            listeButton[i][j].setIcon(redimensionner(icon));
                    }
                }
            }
        }
        if(e.getSource() == bPion) {
            ImageIcon icon = chargerImage();
            lPion.setIcon(redimensionner(icon));
            for(int i = 0; i < listeButton.length; i++) {
                for(int j = 0; j < listeButton[i].length; j++) {
                    if(listePieces[i][j] != null) {
                        if (listePieces[i][j].getType().equals("Pion"))
                            listeButton[i][j].setIcon(redimensionner(icon));
                    }
                }
            }
        }
        if(e.getSource() == bCouleur) {
            Color initialBackground = pPrevisu.getBackground();
            Color background = JColorChooser.showDialog(null, "Choisir une couleur pour les cases", initialBackground);
            if (background != null) {
                pPrevisu.setBackground(background);
                for(int i = 0; i < listeButton.length; i++) {
                    for(int j = 0; j < listeButton[i].length; j++) {
                        if(listeButton[i][j].getBackground() != Color.WHITE)
                            listeButton[i][j].setBackground(background);
                    }
                }
            }
        }
    }

}
