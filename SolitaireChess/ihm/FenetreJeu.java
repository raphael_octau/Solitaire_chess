package SolitaireChess.ihm;

import SolitaireChess.Controleur;
import SolitaireChess.metier.Plateau;
import SolitaireChess.piece.Piece;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

public class FenetreJeu extends JFrame implements ActionListener {

    private Controleur controleur;
    private boolean modeSelection = false;

    private JButton listeButton[][] = new JButton[4][4];

    private JMenuItem itemNouvelle, itemChangerDefi, itemAnnuler, itemRecharger, itemTheme, itemResol, itemRegle, itemAPropos;
    private JButton boutonNouvelle, boutonChangerDefi, boutonAnnuler, boutonRecharger, boutonRegle, boutonResol;
    private JLabel nbDeplacements;
    private JCheckBox cbAide;
    private Color coulActuelle,
                  coulDebutant = new Color(174,224,167),
                  coulIntermediaire = new Color(198,198,255),
                  coulAvance = new Color(215,174,255),
                  coulExpert = new Color(232,142,130),
                  coulPerso = new Color(255,228,178);

    /**
     * Cree une nouvelle fenetre de jeu
     * @param c le controlleur actuel
     */
    public FenetreJeu(Controleur c) {
        this.controleur = c;
        this.coulActuelle = coulDebutant;

        setTitle("Solitaire Chess");
        setIconImage(new ImageIcon("./ressources/Images/cavalier.gif").getImage());
        setResizable(false);

        addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                dispose();
                new FenetreAccueil(controleur);
            }
        });

        //---------- BARE DE MENU ----------//
        JMenuBar menuBar = new JMenuBar();

        //FICHIER
        JMenu menuFichier = new JMenu("Fichier");
        itemNouvelle = new JMenuItem("Nouvelle partie");
        itemNouvelle.setIcon(new ImageIcon("./ressources/Images/nouveau.png"));
        itemNouvelle.setAccelerator(KeyStroke.getKeyStroke("control N"));
        InputMap inputMap = itemNouvelle.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW);
        KeyStroke nouv = KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK);
        inputMap.put(nouv, "Nouvelle");
        itemNouvelle.getActionMap().put("Nouvelle", nouvelle);
        itemChangerDefi = new JMenuItem("Changement de défi");
        itemChangerDefi.setIcon(new ImageIcon("./ressources/Images/changer_defi.png"));
        itemChangerDefi.addActionListener(this);
        itemChangerDefi.setAccelerator(KeyStroke.getKeyStroke("control O"));
        inputMap = itemChangerDefi.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW);
        KeyStroke charg = KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK);
        inputMap.put(charg, "Charger");
        itemChangerDefi.getActionMap().put("Charger", charger);
        menuFichier.add(itemNouvelle);
        menuFichier.add(itemChangerDefi);

        //ACTIONS
        JMenu menuActions = new JMenu("Actions");
        itemAnnuler = new JMenuItem("Annulation");
        itemAnnuler.setAccelerator(KeyStroke.getKeyStroke("control Z"));
        inputMap = itemAnnuler.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW);
        KeyStroke ret = KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_DOWN_MASK);
        inputMap.put(ret, "Retour");
        itemAnnuler.getActionMap().put("Retour", retour);
        itemAnnuler.setIcon(new ImageIcon("./ressources/Images/retour.png"));
        itemResol = new JMenuItem("Affichager la solution");
        itemResol.setIcon(new ImageIcon("./ressources/Images/aide.png"));
        itemResol.addActionListener(this);
        itemResol.setAccelerator(KeyStroke.getKeyStroke("control A"));
        inputMap = itemResol.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW);
        KeyStroke aide = KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_DOWN_MASK);
        inputMap.put(aide, "Solution");
        itemResol.getActionMap().put("Solution", soluce);
        itemRecharger = new JMenuItem("Recharger");
        itemRecharger.setIcon(new ImageIcon("./ressources/Images/recharger.png"));
        itemRecharger.setAccelerator(KeyStroke.getKeyStroke("control R"));
        inputMap = itemRecharger.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW);
        KeyStroke rech = KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK);
        inputMap.put(rech, "Recharger");
        itemRecharger.getActionMap().put("Recharger", recharger);
        menuActions.add(itemResol);
        menuActions.addSeparator();
        menuActions.add(itemRecharger);
        menuActions.add(itemAnnuler);

        //THEMES
        JMenu menuThemes = new JMenu("Thèmes");
        itemTheme = new JMenuItem("Choisir un thème");
        itemTheme.addActionListener(this);
        menuThemes.add(itemTheme);

        //AIDE
        JMenu menuAide = new JMenu("Aide");
        cbAide = new JCheckBox("Activer les aides", true);
        cbAide.addActionListener(this);
        itemRegle = new JMenuItem("Affichager les règles");
        itemRegle.setIcon(new ImageIcon("./ressources/Images/regles.png"));
        itemRegle.addActionListener(this);
        itemRegle.setAccelerator(KeyStroke.getKeyStroke("F1"));
        inputMap = itemRegle.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW);
        KeyStroke regle = KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0);
        inputMap.put(regle, "Retour");
        itemRegle.getActionMap().put("Retour", regles);
        itemAPropos = new JMenuItem("A propos");
        itemAPropos.setIcon(new ImageIcon("./ressources/Images/a_propos.png"));
        itemAPropos.addActionListener(this);
        menuAide.add(cbAide);
        menuAide.add(itemRegle);
        menuAide.add(itemAPropos);

        menuBar.add(menuFichier);
        menuBar.add(menuActions);
        menuBar.add(menuThemes);
        menuBar.add(menuAide);
        setJMenuBar(menuBar);

        //---------- BARE D'OUTILS ----------//
        JToolBar toolbar = new JToolBar();
        boutonNouvelle = new JButton(new ImageIcon("./ressources/Images/nouveau.png"));
        boutonNouvelle.addActionListener(this);
        boutonNouvelle.setToolTipText("Nouvelle partie (Ctrl-N)");
        boutonNouvelle.setPreferredSize(new Dimension(24, 24));
        boutonChangerDefi = new JButton(new ImageIcon("./ressources/Images/changer_defi.png"));
        boutonChangerDefi.addActionListener(this);
        boutonChangerDefi.setToolTipText("Changer de défi (Ctrl-O)");
        boutonAnnuler = new JButton(new ImageIcon("./ressources/Images/retour.png"));
        boutonAnnuler.addActionListener(this);
        boutonAnnuler.setToolTipText("Retour (Ctrl-Z)");
        boutonRecharger = new JButton(new ImageIcon("./ressources/Images/recharger.png"));
        boutonRecharger.addActionListener(this);
        boutonRecharger.setToolTipText("Recharger (Ctrl-R)");
        boutonRegle = new JButton(new ImageIcon("./ressources/Images/regles.png"));
        boutonRegle.addActionListener(this);
        boutonRegle.setToolTipText("Afficher les règles (F1)");
        boutonResol = new JButton(new ImageIcon("./ressources/Images/aide.png"));
        boutonResol.addActionListener(this);
        boutonResol.setToolTipText("Afficher la solution");

        toolbar.add(boutonNouvelle);
        toolbar.add(boutonChangerDefi);
        toolbar.addSeparator();
        toolbar.add(boutonRecharger);
        toolbar.add(boutonAnnuler);
        toolbar.addSeparator();
        toolbar.add(boutonResol);
        toolbar.addSeparator();
        toolbar.add(boutonRegle);

        toolbar.setFloatable(false);
        getContentPane().add(toolbar, BorderLayout.PAGE_START);

        //---------- GRILLE ----------//
        JPanel grille = new JPanel(new GridLayout(4, 4));
        grille.setBorder(BorderFactory.createLoweredBevelBorder());

        for(int i=0; i<listeButton.length; i++){
            for(int j=0; j<listeButton.length; j++){
                listeButton[i][j]= new JButton();
                listeButton[i][j].setPreferredSize(new Dimension(100, 100));
                listeButton[i][j].setFocusPainted(false);
                final int iFinal = i;
                final int jFinal = j;
                listeButton[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(contientPiece(iFinal, jFinal)) {
                            modeSelection = !modeSelection;
                            colorerGrille();
                            controleur.deplacer(iFinal, jFinal);
                        } else {
                            colorerGrille();
                            modeSelection = false;
                        }
                    }
                });

                if ((i + j) % 2 == 0) listeButton[i][j].setBackground(Color.WHITE);
                else listeButton[i][j].setBackground(coulDebutant);

                grille.add(listeButton[i][j]);
            }
        }
        add(grille);

        colorerGrille();


        //---------- INFOS ----------//
        JPanel pInfos = new JPanel(new FlowLayout());
        pInfos.setBorder(BorderFactory.createMatteBorder(2, 0, 0, 0, Color.DARK_GRAY));
        nbDeplacements = new JLabel("0 déplacements");
        pInfos.add(nbDeplacements);

        add(pInfos, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * Recolore le damier de la grille
     */
    private void colorerGrille(){
        for(int i=0; i<listeButton.length; i++){
            for(int j=0; j<listeButton.length; j++){
                if ((i + j) % 2 == 0) listeButton[i][j].setBackground(Color.WHITE);
                else listeButton[i][j].setBackground(coulActuelle);
                listeButton[i][j].setBorder(null);
            }
        }
    }

    /**
     * Remet la selection de case a zero et recolore la grille
     * Dans le cas d'un clique sur Aide
     */
    public void reinitAide() {
        colorerGrille();
        modeSelection = false;
    }

    /**
     * Les Actions associées aux raccourcis clavier
     */
    Action retour = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            boutonAnnuler.doClick();
        }
    };
    Action recharger = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            boutonRecharger.doClick();
        }
    };
    Action regles = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            boutonRegle.doClick();
        }
    };
    Action nouvelle = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            boutonNouvelle.doClick();
        }
    };
    Action charger = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            boutonChangerDefi.doClick();
        }
    };
    Action soluce = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            boutonResol.doClick();
        }
    };

    /**
     * Definit la couleur de la grille en fonction du niveau
     * @param niv le niveau actuel
     */
    public void majCoul(int niv) {
        if((double)(niv)/15 <= 1)
            coulActuelle = coulDebutant;
        else if((double)(niv)/15 <= 2)
            coulActuelle = coulIntermediaire;
        else if((double)(niv)/15 <= 3)
            coulActuelle = coulAvance;
        else if((double)(niv)/15 <= 4)
            coulActuelle = coulExpert;
        else
            coulActuelle = coulPerso;
        if(controleur.getCoul() != null)
            coulActuelle = controleur.getCoul();
        colorerGrille();
    }

    /**
     * Actualise la position des pions
     * @param p le plateau actuel
     */
    public void actualiser(Plateau p) {
        Piece[][] tabPieces = p.getTabPieces();

        for (int ligne = 0;ligne < tabPieces.length; ligne++ ) {
            for (int colonne = 0; colonne < tabPieces[ligne].length; colonne++) {
                if(tabPieces[ligne][colonne] == null) {
                    listeButton[ligne][colonne].setIcon(null);
                }
                else if(tabPieces[ligne][colonne].getType().equals("Tour")) {
                    listeButton[ligne][colonne].setIcon(controleur.getImgTour());
                }
                else if(tabPieces[ligne][colonne].getType().equals("Roi")) {
                    listeButton[ligne][colonne].setIcon(controleur.getImgRoi());
                }
                else if(tabPieces[ligne][colonne].getType().equals("Fou")) {
                    listeButton[ligne][colonne].setIcon(controleur.getImgFou());
                }
                else if(tabPieces[ligne][colonne].getType().equals("Dame")) {
                    listeButton[ligne][colonne].setIcon(controleur.getImgDame());
                }
                else if(tabPieces[ligne][colonne].getType().equals("Cavalier")) {
                    listeButton[ligne][colonne].setIcon(controleur.getImgCavalier());
                }
                else if(tabPieces[ligne][colonne].getType().equals("Pion")) {
                    listeButton[ligne][colonne].setIcon(controleur.getImgPion());
                }
            }
        }

    }

    /**
     * Affiche une boite de dialogue et gere recommencer ou suivant
     */
    public void gagne() {
        //Custom button text
        Object[] options = {"Recommencer", "Niveau suivant"};
        int n = JOptionPane.showOptionDialog(this,
                "Partie gagnée en " + nbDeplacements.getText() +  " !",
                "Défi réussi !",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        if(n == 1)
            controleur.nivSuivant();
        else if(n == 0)
            controleur.nouvNiveau(controleur.getPlateau().getNiveau());
    }

    /**
     * Met a jour le nombre de deplacements
     * @param nb le nombre de deplacements
     */
    public void setNbDeplacements(int nb) {
        nbDeplacements.setText(nb + " déplacements");
    }

    /**
     * Met a jour le niveau
     * @param niv le niveau
     */
    public void setNiveau(String niv) {
        setTitle("Solitaire Chess - " + niv);
    }

    /**
     * Ajoute une bordure pour une case ou le deplacement est possible
     * @param x,y les coordonnees de la case
     */
    public void setCouleurPossible(int x, int y) {
        listeButton[x][y].setBorder(new LineBorder(Color.DARK_GRAY, 3));
    }

    /**
     * Ajoute une bordure pour une case ou le deplacement est possible et vide
     * @param x,y les coordonnees de la case
     */
    public void setCouleurPossibleVide(int x, int y) {
        listeButton[x][y].setBorder(new LineBorder(Color.DARK_GRAY, 1));
    }

    /**
     * Ajoute une bordure pour une case ou le deplacement est commencé
     * @param x,y les coordonnees de la case
     */
    public void setCouleurDepart(int x, int y) {
        listeButton[x][y].setBorder(new LineBorder(Color.ORANGE, 4));
    }

    /**
     * @return si on a selectionne deja une case ou non
     */
    public boolean getModeSelection() {
        return modeSelection;
    }

    /**
     * @param x,y les coordonnees
     * @return si il y a une piece sur cette case
     */
    public boolean contientPiece(int x, int y) {
        return listeButton[x][y].getIcon() != null;
    }

    /**
     * Ecoute les actions sur les boutons de menu ou d'outils
     * @param e
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == boutonRegle || e.getSource() == itemRegle){
        	controleur.ouvrirAide();
        }
        if(e.getSource() == boutonRecharger || e.getSource() == itemRecharger)
            controleur.recharger();
        if(e.getSource() == boutonAnnuler || e.getSource() == itemAnnuler)
            controleur.retour();
        if(e.getSource() == boutonChangerDefi || e.getSource() == itemChangerDefi)
            controleur.choisirNiveau();
        if(e.getSource() == cbAide) {
            controleur.setAide(cbAide.isSelected());
        }
        if(e.getSource() == itemTheme) {
            new FenetreTheme(controleur);
        }
        if(e.getSource() == this.boutonResol || e.getSource() == this.itemResol)
        {
            colorerGrille();
            modeSelection = false;
            controleur.resol();
        }
        if(e.getSource() == itemNouvelle || e.getSource() == boutonNouvelle) {
            int res = JOptionPane.showConfirmDialog(this,
                    "Etes-vous sûr de vouloir créer une nouvelle partie?\nToute progression sera perdue.",
                    "Nouvelle partie",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (res == JOptionPane.YES_OPTION) {
                controleur.nouvellePartie();
            }else if (res == JOptionPane.NO_OPTION){
                return;
            }
        }
        if(e.getSource() == itemAPropos) {
            JOptionPane.showMessageDialog(this, "Réalisé par Alexia Dauvergne, Raphaël Octau, Antoine Prevost et Steven Yon" +
                            "\nDans le cadre du projet tutoré du second semestre\nà l'IUT Informatique du Havre. Année 2015-2016.",
                    "A propos", JOptionPane.INFORMATION_MESSAGE, new ImageIcon("./ressources/Images/cavalier.gif"));
        }
    }

}
