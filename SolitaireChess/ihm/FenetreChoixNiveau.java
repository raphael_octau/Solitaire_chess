package SolitaireChess.ihm;

import SolitaireChess.Controleur;
import SolitaireChess.metier.Plateau;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class FenetreChoixNiveau extends JFrame implements ActionListener, MouseListener {

    private ArrayList<JButton> alButton = new ArrayList<JButton>();
    private String niveau = "";
    private String difficulte = "";
    private JButton listeButton[][] = new JButton[4][4];
    private JLabel info;
    private Plateau p;
    private Controleur controleur;
    private Color coulDebutant = new Color(174,224,167),
            coulIntermediaire = new Color(198,198,255),
            coulAvance = new Color(215,174,255),
            coulExpert = new Color(232,142,130),
            coulPerso = new Color(255,228,178);

    /**
     * Cree une nouvelle Fenetre pour charger un niveau deja debloque
     * @param p le PLateau actuel
     * @param controleur le controleur actuel
     */
    public FenetreChoixNiveau(Plateau p, Controleur controleur) {
        this.p = p;
        this.controleur = controleur;

        setLocation(100, 100);
        setResizable(false);
        setTitle("Choix du niveau");
        setIconImage(new ImageIcon("./ressources/Images/cavalier.gif").getImage());
        setLayout(new GridBagLayout());

        JPanel listes = new JPanel(new GridLayout(1, 4));

        JPanel p1, p2, p3, p4, p5;
        p1 = new JPanel( new GridLayout(16, 1, 0, 5));
        p1.setBorder(new EmptyBorder(5, 5, 5, 5));
        p1.setBackground(coulDebutant);
        p2 = new JPanel( new GridLayout(16, 1, 0, 5));
        p2.setBorder(new EmptyBorder(5, 5, 5, 5));
        p2.setBackground(coulIntermediaire);
        p3 = new JPanel( new GridLayout(16, 1, 0, 5));
        p3.setBorder(new EmptyBorder(5, 5, 5, 5));
        p3.setBackground(coulAvance);
        p4 = new JPanel( new GridLayout(16, 1, 0, 5));
        p4.setBorder(new EmptyBorder(5, 5, 5, 5));
        p4.setBackground(coulExpert);
        p5 = new JPanel( new GridLayout(16, 1, 0, 5));
        p5.setBorder(new EmptyBorder(5, 5, 5, 5));
        p5.setBackground(coulPerso);

        JLabel l1, l2, l3, l4, l5;
        l1 = new JLabel("Débutant",         SwingConstants.CENTER);
        l2 = new JLabel("Intermédiaire",    SwingConstants.CENTER);
        l3 = new JLabel("Avancé",           SwingConstants.CENTER);
        l4 = new JLabel("Expert",           SwingConstants.CENTER);
        l5 = new JLabel("Personnalisé",     SwingConstants.CENTER);

        p1.add(l1);
        p2.add(l2);
        p3.add(l3);
        p4.add(l4);
        p5.add(l5);

        for(int i = 0; i<76; i++){
            JButton b;

            if(i<16 && i>0 ){
                b = new JButton("Niveau " + Integer.toString(i));
                p1.add(b);
                alButton.add(b);
            }
            if(i<31 && i>15){
                b = new JButton("Niveau " + Integer.toString(i));
                p2.add(b);
                alButton.add(b);
            }
            if(i<46 && i>30){
                b = new JButton("Niveau " + Integer.toString(i));
                p3.add(b);
                alButton.add(b);
            }
            if(i<61 && i>45){
                b = new JButton("Niveau " + Integer.toString(i));
                p4.add(b);
                alButton.add(b);
            }
            if(i<76 && i>60){
                b = new JButton("Niveau " + Integer.toString(i));
                p5.add(b);
                alButton.add(b);
            }
        }

        for(int i = 0; i<75; i++) {
            alButton.get(i).setEnabled(false);
            alButton.get(i).addActionListener(this);
            alButton.get(i).setPreferredSize(new Dimension(100, 30));
            alButton.get(i).addMouseListener(this);
        }

        listes.add(p1);
        listes.add(p2);
        listes.add(p3);
        listes.add(p4);
        listes.add(p5);
        add(listes);

        initNiveauxDecouverts();

        //---------- GRILLE ----------//
        JPanel ensGrille = new JPanel(new BorderLayout());

        info = new JLabel("Survolez un niveau débloqué pour le visualiser");
        info.setHorizontalAlignment(SwingConstants.CENTER);

        ensGrille.add(info, BorderLayout.NORTH);

        JPanel grille = new JPanel(new GridLayout(4, 4));
        grille.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(20, 20, 20, 20), new LineBorder(Color.DARK_GRAY, 2)));

        for(int i=0; i<listeButton.length; i++){
            for(int j=0; j<listeButton.length; j++){
                listeButton[i][j]= new JButton();
                listeButton[i][j].setPreferredSize(new Dimension(100, 100));
                listeButton[i][j].setBorderPainted( false );
                listeButton[i][j].setFocusPainted( false );

                if ((i + j) % 2 == 0) listeButton[i][j].setBackground(Color.WHITE);
                else listeButton[i][j].setBackground(Color.LIGHT_GRAY);

                grille.add(listeButton[i][j]);
            }
        }
        ensGrille.add(grille);

        add(ensGrille);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * @return le niveau choisi
     */
    public int getNiveau() { return Integer.parseInt(this.niveau); }

    /**
     * Initialise la liste des niveaux debloques
     * depuis les fichiers Niveaux.txt et NiveauxPerso.txt
     */
    private void initNiveauxDecouverts() {
        try {

            Scanner sc;

            sc = new Scanner(new FileReader("./ressources/Niveaux.txt"));

            int cpt = 0;
            int nbValide = 0;

            while(sc.hasNextLine() && cpt < 60){
                String ligne = sc.next();

                if(ligne.contains("*")) {
                    alButton.get(cpt).setEnabled(true);
                    if(cpt < 15 && nbValide >= 7) {
                        debloquerDifficulte('I');
                        nbValide = 0;
                    } else if(cpt < 30 && nbValide >= 7) {
                        debloquerDifficulte('A');
                        nbValide = 0;
                    } else if(cpt < 45 && nbValide >= 7) {
                        debloquerDifficulte('E');
                        nbValide = 0;
                    }
                    nbValide++;
                }
                cpt++;
            }
        }catch(IOException e) {
            System.out.println(e);
        }

        //Niveaux Perso
        try {
            BufferedReader reader = new BufferedReader(new FileReader("./ressources/NiveauxPerso.txt"));
            int lines = 0;
            while (reader.readLine() != null) lines++;
            reader.close();

            for(int i = 0; i < lines-1; i++)
                alButton.get(i+60).setEnabled(true);
        } catch (IOException e) {
            System.out.println("Erreur lors de la sauvegarde du niveau");
        }
    }

    /**
     * Verifie si +50% des niveaux d'une difficulte
     * et debloque la difficulte suivante
     * @param difficulte la difficulte a debloquer
     */
    private void debloquerDifficulte(char difficulte) {
        switch (difficulte) {
            case 'I' :
                alButton.get(15).setEnabled(true);
                break;
            case 'A' :
                alButton.get(45).setEnabled(true);
                break;
            case 'E' :
                alButton.get(60).setEnabled(true);
                break;
        }
    }

    /**
     * Affiche un appercu du niveau
     * Methode appelee lors du suvol d'un bouton par la souris
     * @param niv le niveau a afficher
     */
    private void chargerNiveaux(int niv) {
        try {

            Scanner sc;

            sc = new Scanner(new FileReader("./ressources/Niveaux.txt"));

            int niveau = niv;

            Color coul;
            if((double)(niveau)/15 <= 1)
                coul = coulDebutant;
            else if((double)(niveau)/15 <= 2)
                coul = coulIntermediaire;
            else if((double)(niveau)/15 <= 3)
                coul = coulAvance;
            else if((double)(niveau)/15 <= 4)
                coul = coulExpert;
            else
                coul = coulPerso;

            if(controleur.getCoul() != null)
                coul = controleur.getCoul();

            for(int i=0; i<listeButton.length; i++){
                for(int j=0; j<listeButton.length; j++){
                    if ((i + j) % 2 == 0) listeButton[i][j].setBackground(Color.WHITE);
                    else listeButton[i][j].setBackground(coul);
                }
            }

            int cpt = 1;

            while(sc.hasNextLine() && cpt <= 60){
                String ligne = sc.next();

                if(cpt == niveau){

                    String[] s = ligne.split("(?<=\\G.{3})");

                    for(String str : s){
                        if(!str.contains("/") & !str.contains("*")) {
                            int l = Character.getNumericValue(str.charAt(1));
                            int c = Character.getNumericValue(str.charAt(2));
                            switch (str.charAt(0)) {
                                case 'P':
                                    listeButton[l][c].setIcon(controleur.getImgPion());
                                    break;
                                case 'T':
                                    listeButton[l][c].setIcon(controleur.getImgTour());
                                    break;
                                case 'R':
                                    listeButton[l][c].setIcon(controleur.getImgRoi());
                                    break;
                                case 'D':
                                    listeButton[l][c].setIcon(controleur.getImgDame());
                                    break;
                                case 'F':
                                    listeButton[l][c].setIcon(controleur.getImgFou());
                                    break;
                                case 'C':
                                    listeButton[l][c].setIcon(controleur.getImgCavalier());
                                    break;
                            }
                        }
                    }

                }
                cpt++;
            }
        }catch(IOException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Affiche un appercu du niveau perso
     * Methode appelee lors du suvol d'un bouton par la souris
     * @param niv le niveau perso a afficher
     */
    private void chargerNiveauxPerso(int niv) {
        try {

            Scanner sc;

            sc = new Scanner(new FileReader("./ressources/NiveauxPerso.txt"));

            int niveau = niv;

            Color coul = coulPerso;
            if(controleur.getCoul() != null)
                coul = controleur.getCoul();

            for(int i=0; i<listeButton.length; i++){
                for(int j=0; j<listeButton.length; j++){
                    if ((i + j) % 2 == 0) listeButton[i][j].setBackground(Color.WHITE);
                    else listeButton[i][j].setBackground(coul);
                }
            }

            int cpt = 1;

            while(sc.hasNextLine() && cpt <= 15){
                String ligne = sc.next();

                if(cpt == niveau-60){

                    String[] s = ligne.split("(?<=\\G.{3})");

                    for(String str : s){
                        if(!str.contains("/") & !str.contains("*")) {
                            int l = Character.getNumericValue(str.charAt(1));
                            int c = Character.getNumericValue(str.charAt(2));
                            switch (str.charAt(0)) {
                                case 'P':
                                    listeButton[l][c].setIcon(controleur.getImgPion());
                                    break;
                                case 'T':
                                    listeButton[l][c].setIcon(controleur.getImgTour());
                                    break;
                                case 'R':
                                    listeButton[l][c].setIcon(controleur.getImgRoi());
                                    break;
                                case 'D':
                                    listeButton[l][c].setIcon(controleur.getImgDame());
                                    break;
                                case 'F':
                                    listeButton[l][c].setIcon(controleur.getImgFou());
                                    break;
                                case 'C':
                                    listeButton[l][c].setIcon(controleur.getImgCavalier());
                                    break;
                            }
                        }
                    }

                }
                cpt++;
            }
        }catch(IOException ex) {
            System.out.println(ex);
        }
    }

    /**
     * Ecoute les actions sur les boutons des listes pour charger un niveau
     */
    public void actionPerformed(ActionEvent e) {
        for(int i = 0; i < alButton.size(); i++) {
            if(i>=0 && i<16){
                this.difficulte = "Débutant";
            }
            if(i>=15 && i<31){
                this.difficulte = "Intermediaire";
            }
            if(i>=30 && i<46){
                this.difficulte = "Avancé";
            }
            if(i>=45 && i<61){
                this.difficulte = "Expert";
            }
            if(i>=60 && i<76){
                this.difficulte = "Personnalisé";
            }

            if(e.getSource() == alButton.get(i)) {
                this.niveau = alButton.get(i).getText().split(" ")[1];
                controleur.nouvNiveau(getNiveau());
                this.dispose();
                break;
            }
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Quand on survole un bouton, on affiche le niveau associé
     */
    public void mouseEntered(MouseEvent e) {
        JButton btn = (JButton) e.getSource();

        if(!btn.isEnabled())
            return;

        info.setText(btn.getText());

        for (int ligne = 0;ligne < listeButton.length; ligne++)
            for (int colonne = 0; colonne < listeButton[ligne].length; colonne++)
                listeButton[ligne][colonne].setIcon(null);

        int niv = Integer.parseInt(btn.getText().split(" ")[1]);
        if(niv <= 60)
            chargerNiveaux(niv);
        else
            chargerNiveauxPerso(niv);
    }

    /**
     * Quand on sors la souris d'un bouton on reinit la grille d'appercu
     */
    public void mouseExited(MouseEvent e) {
        info.setText("Survolez un niveau débloqué pour le visualiser");

        for (int ligne = 0;ligne < listeButton.length; ligne++) {
            for (int colonne = 0; colonne < listeButton[ligne].length; colonne++) {
                listeButton[ligne][colonne].setIcon(null);
                if ((ligne + colonne) % 2 == 0) listeButton[ligne][colonne].setBackground(Color.WHITE);
                else listeButton[ligne][colonne].setBackground(Color.LIGHT_GRAY);
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
    }
}
