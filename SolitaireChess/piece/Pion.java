package SolitaireChess.piece;

import SolitaireChess.metier.Position;

import java.util.ArrayList;

public class Pion extends Piece
{

    /**
     * Cree un nouveau Pion
     * @param pos la position ou il se trouve
     */
    public Pion (Position pos) { super(pos); }
    public Pion(int x, int y) { super(x, y); }

    /**
     * @return TOUS les deplacements possibles
     */
    public ArrayList<Position> setDeplacement()
    {
        this.deplacement = new ArrayList<Position>();

        this.deplacement.add(new Position(this.pos.getX()-1,this.pos.getY()-1));
        this.deplacement.add(new Position(this.pos.getX()-1,this.pos.getY()+1));

        return this.deplacement;
    }

    /**
     * @param tab le tableau de pieces du PLateau
     * @return UNIQUEMENT les deplacements possibles sur des pieces
     */
    public ArrayList<Position> updateDeplacement(Piece[][] tab)
    {
        ArrayList<Position> vraiDepl = new ArrayList<Position>();

        for(Position p : deplacement) {
            if(p.getX() >= 0 && p.getY() >= 0 && p.getX() < 4 && p.getY() < 4 && tab[p.getX()][p.getY()] != null)
                vraiDepl.add(p);
        }

        return vraiDepl;
    }

    /**
     * @return le type de piece
     */
    public String getType() {
        return "Pion"; }
}