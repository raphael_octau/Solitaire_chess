package SolitaireChess.piece;

import SolitaireChess.metier.Position;

import java.util.ArrayList;

public class Fou extends Piece
{

    /**
     * Cree un nouveau Fou
     * @param pos la position ou il se trouve
     */
    public Fou (Position pos)
    {
        super(pos);
    }
    
    public Fou(int x, int y)
    {
        super(new Position(x,y));
    }

    /**
     * @return TOUS les deplacements possibles
     */
    public ArrayList<Position> setDeplacement()
    {
        this.deplacement = new ArrayList<Position>();

        for(int i=1; i<=3; i++)
        {
            this.deplacement.add(new Position(this.pos.getX()-i,this.pos.getY()-i));
            this.deplacement.add(new Position(this.pos.getX()+i,this.pos.getY()+i));
            this.deplacement.add(new Position(this.pos.getX()-i,this.pos.getY()+i));
            this.deplacement.add(new Position(this.pos.getX()+i,this.pos.getY()-i));
        }

        return this.deplacement;
     
    }

    /**
     * @param tab le tableau de pieces du PLateau
     * @return UNIQUEMENT les deplacements possibles sur des pieces
     */
    public ArrayList<Position> updateDeplacement(Piece[][] tab)
    {
        this.deplacement.clear();

        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX()-i >= 0 && this.getPos().getY()-i >= 0 && this.getPos().getX()-i < 4 && this.getPos().getY()-i < 4 && tab[this.getPos().getX()-i][this.getPos().getY()-i] != null) {
                this.deplacement.add(new Position(this.getPos().getX() - i, this.getPos().getY() - i));
                break;
            }
        }
        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX()+i >= 0 && this.getPos().getY()+i >= 0 && this.getPos().getX()+i < 4 && this.getPos().getY()+i < 4 && tab[this.getPos().getX()+i][this.getPos().getY()+i] != null) {
                this.deplacement.add(new Position(this.getPos().getX() + i, this.getPos().getY() + i));
                break;
            }
        }
        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX()-i >= 0 && this.getPos().getY()+i >= 0 && this.getPos().getX()-i < 4 && this.getPos().getY()+i < 4 && tab[this.getPos().getX()-i][this.getPos().getY()+i] != null) {
                this.deplacement.add(new Position(this.getPos().getX() - i, this.getPos().getY() + i));
                break;
            }
        }
        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX()+i >= 0 && this.getPos().getY()-i >= 0 && this.getPos().getX()+i < 4 && this.getPos().getY()-i < 4 && tab[this.getPos().getX()+i][this.getPos().getY()-i] != null) {
                this.deplacement.add(new Position(this.getPos().getX() + i, this.getPos().getY() - i));
                break;
            }
        }

        return this.deplacement;

    }

    /**
     * @return le type de piece
     */
    public String getType() {
        return "Fou"; }

}