package SolitaireChess.piece;

import SolitaireChess.metier.Position;

import java.util.ArrayList;

public abstract class Piece {

    public Position pos;
    public ArrayList<Position> deplacement;

    /**
     * Cree une nouvelle Piece
     * @param pos la position ou elle se trouve
     */
    protected Piece (Position pos) {
        this.pos = pos;
        this.deplacement = new ArrayList<Position>();
        this.setDeplacement();
    }
    protected Piece(int x, int y) {
        this(new Position(x,y));
    }

    /**
     * @return la position de la Piece
     */
    public Position getPos(){return this.pos;}

    /**
     * @return la liste des deplacements possibles
     */
    public abstract ArrayList<Position> setDeplacement();

    /**
     * @param tab le tableau des pieces du Plateau
     * @return les deplacements possibles ou il y a une piece
     */
    public abstract ArrayList<Position> updateDeplacement(Piece[][] tab);

    /**
     * Definit une nouvelle position
     * @param x,y les coordonnees
     */
    public void setPos(int x, int y) { this.pos = new Position(x, y); }

    /**
     * @return le type de piece
     */
    public abstract String getType();

}
