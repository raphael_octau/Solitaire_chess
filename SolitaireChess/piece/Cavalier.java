package SolitaireChess.piece;

import SolitaireChess.metier.Position;

import java.util.ArrayList;

public class Cavalier extends Piece
{

    /**
     * Cree un nouveau Cavalier
     * @param pos la position ou il se trouve
     */
    public Cavalier (Position pos) { super(pos); }
    public Cavalier(int x, int y) { super(x, y); }

    /**
     * @return TOUS les deplacements possibles
     */
    public ArrayList<Position> setDeplacement()
    {
        this.deplacement = new ArrayList<Position>();

        this.deplacement.add(new Position(this.pos.getX()-2,this.pos.getY()-1));
        this.deplacement.add(new Position(this.pos.getX()-2,this.pos.getY()+1));
        
        this.deplacement.add(new Position(this.pos.getX()+2,this.pos.getY()-1));
        this.deplacement.add(new Position(this.pos.getX()+2,this.pos.getY()+1));
        
        this.deplacement.add(new Position(this.pos.getX()-1,this.pos.getY()-2));
        this.deplacement.add(new Position(this.pos.getX()+1,this.pos.getY()-2));
        
        this.deplacement.add(new Position(this.pos.getX()-1,this.pos.getY()+2));
        this.deplacement.add(new Position(this.pos.getX()+1,this.pos.getY()+2));

        return this.deplacement;
    }

    /**
     * @param tab le tableau de pieces du PLateau
     * @return UNIQUEMENT les deplacements possibles sur des pieces
     */
    public ArrayList<Position> updateDeplacement(Piece[][] tab)
    {
        ArrayList<Position> vraiDepl = new ArrayList<Position>();

        for(Position p : deplacement) {
            if(p.getX() >= 0 && p.getY() >= 0 && p.getX() < 4 && p.getY() < 4 && tab[p.getX()][p.getY()] != null)
                vraiDepl.add(p);
        }

        return vraiDepl;

    }

    /**
     * @return le type de piece
     */
    public String getType() {
        return "Cavalier"; }
}