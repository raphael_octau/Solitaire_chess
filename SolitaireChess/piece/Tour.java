package SolitaireChess.piece;

import SolitaireChess.metier.Position;

import java.util.ArrayList;

public class Tour extends Pion
{

    /**
     * Cree un nouveau Tour
     * @param pos la position ou il se trouve
     */
    public Tour (Position pos)
    {
        super(pos);
    }
    public Tour (int x, int y)
    {
        super(new Position(x,y));
    }

    /**
     * @return TOUS les deplacements possibles
     */
    public ArrayList<Position> setDeplacement()
    {
        this.deplacement = new ArrayList<Position>();

        for(int i=1; i<=3; i++)
        {
            this.deplacement.add(new Position(this.pos.getX(),this.pos.getY()-i));
            this.deplacement.add(new Position(this.pos.getX(),this.pos.getY()+i));
            this.deplacement.add(new Position(this.pos.getX()-i,this.pos.getY()));
            this.deplacement.add(new Position(this.pos.getX()+i,this.pos.getY()));
        }

        return this.deplacement;
    }

    /**
     * @param tab le tableau de pieces du PLateau
     * @return UNIQUEMENT les deplacements possibles sur des pieces
     */
    public ArrayList<Position> updateDeplacement(Piece[][] tab)
    {
        this.deplacement.clear();

        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX()-i >= 0 && this.getPos().getY() >= 0 && this.getPos().getX()-i < 4 && this.getPos().getY() < 4 && tab[this.getPos().getX()-i][this.getPos().getY()] != null) {
                this.deplacement.add(new Position(this.getPos().getX() - i, this.getPos().getY()));
                break;
            }
        }
        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX()+i >= 0 && this.getPos().getY() >= 0 && this.getPos().getX()+i < 4 && this.getPos().getY() < 4 && tab[this.getPos().getX()+i][this.getPos().getY()] != null) {
                this.deplacement.add(new Position(this.getPos().getX() + i, this.getPos().getY()));
                break;
            }
        }
        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX() >= 0 && this.getPos().getY()-i >= 0 && this.getPos().getX() < 4 && this.getPos().getY()-i < 4 && tab[this.getPos().getX()][this.getPos().getY()-i] != null) {
                this.deplacement.add(new Position(this.getPos().getX(), this.getPos().getY() - i));
                break;
            }
        }
        for(int i = 1; i <= 3; i++) {
            if(this.getPos().getX() >= 0 && this.getPos().getY()+i >= 0 && this.getPos().getX() < 4 && this.getPos().getY()+i < 4 && tab[this.getPos().getX()][this.getPos().getY()+i] != null) {
                this.deplacement.add(new Position(this.getPos().getX(), this.getPos().getY() + i));
                break;
            }
        }

        return this.deplacement;

    }

    /**
     * @return le type de piece
     */
    public String getType() {
        return "Tour"; }

}